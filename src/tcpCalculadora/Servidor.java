package tcpCalculadora;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Semaphore;

public class Servidor extends Thread {
    public Semaphore semaphore;
    public ServerSocket servidor;
    public boolean estado;

    public Servidor(Semaphore semaphore) {
        this.semaphore = semaphore;
        try {
            this.servidor = new ServerSocket(50005);
            this.estado = false;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public double calculadora(int numero1, int numero2, char operador) {

        switch (operador) {
            case '+':
                return numero1 + numero2;
            case '-':
                return numero1 - numero2;
            case '*':
                return numero1 * numero2;
            case '/':
                return numero1 / numero2;
            default:
                break;
        }
        return 0;
    }

    public void cerrarServidor() {
        this.estado = true;
    }

    @Override
    public void run() {

        try {
            semaphore.acquire();

            System.out.println("Aceptando conexiones...");
            while (!this.estado) {
                Socket cliente = this.servidor.accept();
                System.out.println("Conexión establecida con el cliente con puerto: " + cliente.getPort());
                DataInputStream entrada = new DataInputStream(cliente.getInputStream());
                DataOutputStream salida = new DataOutputStream(cliente.getOutputStream());
                while (true) {
                    //servidor
                    salida.writeUTF("Dime el primer operando o salir:");
                    //cliente
                    String mensaje;
                    int numero1 = 0;
                    mensaje = entrada.readUTF();
                    //servidor
                    if (mensaje.equalsIgnoreCase("salir")) {
                        salida.writeUTF("adios");
                        break;
                    } else {
                        try {
                            numero1 = Integer.parseInt(mensaje);
                        } catch (NumberFormatException e) {
                            System.out.println("Introduciste algo invalido, se asignará al primer valor 10");
                            numero1 = 10;
                        }
                    }
                    salida.writeUTF("Dime el segundo operando:");
                    //cliente
                    int numero2;
                    try {
                        numero2 = Integer.parseInt(entrada.readUTF());
                    } catch (NumberFormatException e) {
                        System.out.println("Introduciste algo invalido, se asignará al segundo valor 10");
                        numero2 = 10;
                    }
                    //servidor
                    salida.writeUTF("Dime el operador [+, -, /, *]:");
                    //cliente
                    char operador;
                    operador = entrada.readUTF().charAt(0);
                    //servidor hacer las operaciones y retornar
                    salida.writeUTF("La respuesta es: " + calculadora(numero1, numero2, operador));

                }

                semaphore.release();
                cliente.close();
                entrada.close();
                salida.close();
                System.out.println("Cerrado streams y sockets, liberando el semáforo");
            }//while estado

            this.servidor.close();

            System.out.println("Cerrado el servidor");

        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("Error al crear el socket");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }//run

}//class
