package tcpCalculadora;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

class Cliente extends Thread {
    public String nombre;
    public Socket cliente;

    public Cliente(String nombre) {
        this.nombre = nombre;
        System.out.println("Creando y conectando el socket stream cliente " + this.nombre);
        try {
            this.cliente = new Socket("localhost", 50005);
        } catch (IOException e) {
            System.out.println("Error al crear el socket");
            e.printStackTrace();
        }
    }

    @Override
    public void run() {

        Scanner sc = new Scanner(System.in);

        try {
            InputStream is = cliente.getInputStream();
            DataInputStream dis = new DataInputStream(is);

            OutputStream os = cliente.getOutputStream();
            DataOutputStream dos = new DataOutputStream(os);

            String mensajeS;
            String mensajeC;
            while (true) {
                //servidor
                mensajeS = dis.readUTF();//"Dime el primer operando o salir:"
                System.out.println(mensajeS);
                //cliente
                mensajeC = sc.nextLine();
                dos.writeUTF(mensajeC);
                //servidor
                mensajeS = dis.readUTF();//"Dime el segundo operando:" o "adios"
                System.out.println(mensajeS);
                if (mensajeS.equalsIgnoreCase("adios")) {
                    break;
                }
                //cliente
                mensajeC = sc.nextLine();
                dos.writeUTF(mensajeC);
                //servidor
                mensajeS = dis.readUTF();//"Dime el operador [+, -, /, *]:"
                System.out.println(mensajeS);
                //cliente
                mensajeC = sc.nextLine();
                boolean a = false;
                while (!a) {
                    if (mensajeC.equals("+") || mensajeC.equals("-") || mensajeC.equals("*") || mensajeC.equals("/")) {
                        dos.writeUTF(mensajeC);
                        a = true;
                    } else {
                        System.out.println("Escriba un operador válido por favor");
                        mensajeC = sc.nextLine();
                    }
                }
                //servidor
                mensajeS = dis.readUTF();//"Respuesta:"
                System.out.println(mensajeS);
            }

            dos.close();
            dis.close();
            cliente.close();
            System.out.println("Cerrado la conexión con cliente " + this.nombre);

        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
