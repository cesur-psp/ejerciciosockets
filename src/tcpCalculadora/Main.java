package tcpCalculadora;

import java.util.concurrent.Semaphore;

public class Main {
    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(5);

        Servidor servidor = new Servidor(semaphore);
        servidor.start();


        Cliente[] clientes = new Cliente[5];
        for (int i = 0; i < clientes.length; i++) {
            clientes[i] = new Cliente("" + (i + 1));
            clientes[i].start();
        }

        for (int i = 0; i < clientes.length; i++) {
            try {
                clientes[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        servidor.cerrarServidor();

    }
}
