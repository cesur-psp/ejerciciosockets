package udpCalculadora;

import java.util.concurrent.Semaphore;

public class mainprueba {
    public static void main(String[] args) {
        int SERVER_PORT = 9876;
        Semaphore semaforo=new Semaphore(1);
// Iniciar el servidor
        ServidorDatagrama server = new ServidorDatagrama(SERVER_PORT,semaforo);
        server.start();

// Iniciar el cliente
        ClienteDatagrama[] client=new ClienteDatagrama[5];
        for (int i=0;i<client.length;i++){
            try {
                client[i] = new ClienteDatagrama("127.0.0.1", SERVER_PORT);
            } catch (Exception e) {
                System.out.println("error al crear el cliente");
            }
        }

        for (int i=0;i<client.length;i++){
            try {
                client[i].start();
                client[i].join();
                System.out.println("Cerrado Cliente "+(i+1));
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }

        server.cerrarServidor();

    }
}
