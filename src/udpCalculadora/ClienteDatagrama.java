package udpCalculadora;

import java.net.*;
import java.util.Scanner;

public class ClienteDatagrama extends Thread {
    public DatagramSocket clientSocket;
    public InetAddress serverAddress;
    public int serverPort;

    public ClienteDatagrama(String serverIP, int serverPort) throws Exception {
        this.serverAddress = InetAddress.getByName(serverIP);
        this.serverPort = serverPort;
        clientSocket = new DatagramSocket();
        System.out.println("creando el cliente");
    }

    @Override
    public void run() {
        Scanner sc = new Scanner(System.in);
        String usuario;
        try {
            while (true) {
                String mensajeInicial = "Hola servidor";
                enviarAlServidor(mensajeInicial);
                // Paso 1: Recibir solicitud del servidor
                String primerSolicitud = receiveStringFromServer(clientSocket);
                System.out.println("Servidor: " + primerSolicitud);

                // Paso 2: Enviar primer operando al servidor
                usuario = sc.nextLine();
                sendToServer(usuario, clientSocket);
                if (usuario.equalsIgnoreCase("salir")||usuario.equalsIgnoreCase("0")) {
                    break;
                }

                // Paso 3: Recibir solicitud del servidor
                String segundaSolicitud = receiveStringFromServer(clientSocket);
                System.out.println("Servidor: " + segundaSolicitud);

                // Paso 4: Enviar segundo operando al servidor
                usuario = sc.nextLine();
                sendToServer(usuario, clientSocket);
                if (usuario.equalsIgnoreCase("salir")||usuario.equalsIgnoreCase("0")) {
                    break;
                }

                // Paso 5: Recibir solicitud del servidor
                String terceraSolicitud = receiveStringFromServer(clientSocket);
                System.out.println("Servidor: " + terceraSolicitud);

                // Paso 6: Enviar operador al servidor
                usuario = sc.nextLine();
                sendToServer(usuario, clientSocket);

                // Paso 7: Recibir resultado del servidor
                int resultado = receiveIntFromServer(clientSocket);
                System.out.println("Servidor: " + resultado);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (clientSocket != null) {
                clientSocket.close();
                System.out.println("Cliente UDP cerrado.");
            }
        }
    }

    public void enviarAlServidor(String mensaje) throws Exception {
        byte[] sendData = mensaje.getBytes();
        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, serverAddress, serverPort);
        clientSocket.send(sendPacket);
    }

    public void sendToServer(String message, DatagramSocket socket) throws Exception {
        byte[] sendData = message.getBytes();
        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, serverAddress, serverPort);
        socket.send(sendPacket);
    }

    public String receiveStringFromServer(DatagramSocket socket) throws Exception {
        byte[] receiveData = new byte[1024];
        DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
        socket.receive(receivePacket);
        return new String(receivePacket.getData(), 0, receivePacket.getLength()).trim();
    }

    public int receiveIntFromServer(DatagramSocket socket) throws Exception {
        byte[] receiveData = new byte[1024];
        DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
        socket.receive(receivePacket);
        return Integer.parseInt(new String(receivePacket.getData(), 0, receivePacket.getLength()).trim());
    }
}
