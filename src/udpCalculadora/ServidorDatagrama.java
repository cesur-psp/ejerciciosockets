package udpCalculadora;

import java.net.*;
import java.util.concurrent.Semaphore;

public class ServidorDatagrama extends Thread {
    public DatagramSocket serverSocket;
    public int port;
    public InetAddress clientAddress;
    public int clientPort;
    public Semaphore semaphore;
    public boolean estado;

    public ServidorDatagrama(int port, Semaphore semaphore) {
        this.port = port;
        this.estado=false;
        this.semaphore=semaphore;
    }

    @Override
    public void run() {
        try {
            serverSocket = new DatagramSocket(port);
            System.out.println("Servidor UDP iniciado en el puerto " + port);
            semaphore.acquire();
            while (true) {
                byte[] receiveData = new byte[1024];
                try {
                    if(!serverSocket.isClosed()){
                        DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                        serverSocket.receive(receivePacket);
                        clientAddress = receivePacket.getAddress();
                        clientPort = receivePacket.getPort();
                    }else{
                        break;
                    }
                }catch (SocketException e){
                    System.out.println("Servidor cerrado");
                    break;
                }

                // Paso 1: Enviar solicitud al cliente
                sendToClient("Dime el primer operando o salir(0)", serverSocket);


                // Paso 2: Recibir primer operando del cliente
                int primerOperando = receiveFromClient(serverSocket);
                if (primerOperando == 0) {
                    System.out.println("adios");
                    semaphore.release();
                    continue;
                }

                // Paso 3: Enviar solicitud al cliente
                sendToClient("Dime el segundo operando o salir(0)", serverSocket);

                // Paso 4: Recibir segundo operando del cliente
                int segundoOperando = receiveFromClient(serverSocket);
                if (segundoOperando == 0) {
                    System.out.println("adios");
                    semaphore.release();
                    continue;
                }

                // Paso 5: Enviar solicitud al cliente
                sendToClient("Dime el operador [+, -, /, *]", serverSocket);

                // Paso 6: Recibir operador del cliente
                String operador = receiveStringFromClient(serverSocket);

                // Paso 7: Realizar operación y enviar resultado al cliente
                int resultado = calcular(primerOperando, segundoOperando, operador);
                sendToClient(String.valueOf(resultado), serverSocket);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void cerrarServidor(){
        serverSocket.close();
    }
    public void sendToClient(String message, DatagramSocket socket) throws Exception {
        byte[] sendData = message.getBytes();
        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, clientAddress, clientPort);
        socket.send(sendPacket);
    }

    public int receiveFromClient(DatagramSocket socket) throws Exception {
        byte[] receiveData = new byte[1024];
        DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
        socket.receive(receivePacket);
        /////////////////
        String mensaje = new String(receivePacket.getData());
        int largo = receivePacket.getLength();
        String nuevo = "";
        for (int i = 0; i < largo; i++) {
            nuevo += mensaje.charAt(i);
        }
        /////////////////
        if (nuevo.equalsIgnoreCase("salir")) {
            return 0;
        } else {
            try {
                return Integer.parseInt(new String(receivePacket.getData(), 0, receivePacket.getLength()));
            } catch (NumberFormatException e) {
                System.out.println("No ha introducido un numero, se asignará el valor 10");
                return 10;
            }
        }


    }

    public String receiveStringFromClient(DatagramSocket socket) throws Exception {
        byte[] receiveData = new byte[1024];
        DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
        socket.receive(receivePacket);
        return new String(receivePacket.getData(), 0, receivePacket.getLength());
    }

    public int calcular(int primerOperando, int segundoOperando, String operador) {
        int resultado = 0;
        switch (operador) {
            case "+":
                resultado = primerOperando + segundoOperando;
                break;
            case "-":
                resultado = primerOperando - segundoOperando;
                break;
            case "*":
                resultado = primerOperando * segundoOperando;
                break;
            case "/":
                resultado = primerOperando / segundoOperando;
                break;
            default:
                System.out.println("Operador inválido. Se asignará +");
                resultado = primerOperando + segundoOperando;
                break;
        }
        return resultado;
    }
}
